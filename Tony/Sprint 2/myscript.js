const form = document.getElementById('form');
const username = document.getElementById('username');
const address = document.getElementById('address');
const email = document.getElementById('email');
const password = document.getElementById('password');
const telephone = document.getElementById('telephone');
const dob = document.getElementById('dob');

form.addEventListener('submit', (e) => {
    e.preventDefault();

    checkInputs();
})

function checkInputs() {
    // get the values from the input
    const usernameValue = username.value.trim();
    const addressValue = address.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const telephoneValue = telephone.value.trim();
    const dobValue = dob.value.trim();

    if(usernameValue === ""){
        setErrorFor(username, "Username cannot be left blank");
    } else {
        setSuccessFor(username);
    }
    if(addressValue === ""){
        setErrorFor(address, "Address cannot be left blank");
    } else {
        setSuccessFor(address);
    }
    if(emailValue === ""){
        setErrorFor(email, "Email cannot be left blank");
    } else {
        setSuccessFor(email);
    }
    if(passwordValue === ""){
        setErrorFor(password, "Password cannot be left blank");
    } else {
        setSuccessFor(password);
    }
    if(telephoneValue === ""){
        setErrorFor(telephone, "Telephone cannot be left blank");
    } else {
        setSuccessFor(telephone);
    }if(dobValue === ""){
        setErrorFor(dob, "Date of Birth cannot be left blank");
    } else {
        setSuccessFor(dob);
    }
}

function setErrorFor(input, message) {
    const formControl = input.parentElement; // .form-control
    const small = formControl.querySelector('small');

    // add error message inside small
    small.innerText = message;

    //add error class
    formControl.className = 'form-control error';
}

function setSuccessFor(input) {
   const formControl = input.parentElement;
   formControl.className = 'form-control success' 
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)I(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])I(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);

}