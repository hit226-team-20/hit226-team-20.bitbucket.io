// https://www.javatpoint.com/oprweb/test.jsp?filename=calculate-age-using-javascript2

function ageCalculator(birthday) 
    var userinput = document.getElementById("birthday").value;  
    var dob = new Date(userinput);  
    
    //calculate month difference from current date in time  
    var month_diff = Date.now() - dob.getTime();  
      
    //convert the calculated difference in date format  
    var age_dt = new Date(month_diff);   
      
    //extract year from date      
    var year = age_dt.getUTCFullYear();  
      
    //now calculate the age of the user  
    var age = Math.abs(year - 1970);  
  
    function check(){
       
        if(age < 13 && age <= 50)
        {
           alert("Get Help");
        }
        else
        {
           alert("You are an adult");
        }
    }