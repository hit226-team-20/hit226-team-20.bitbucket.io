const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');
const phone = document.getElementById('phone');
const birthday = document.getElementById('birthday');

form.addEventListener('submit', e => {
	e.preventDefault();
	
	checkInputs();
});

function checkInputs() {
	// trim to remove the whitespace in user inputs
	const usernameValue = username.value.trim();
	const emailValue = email.value.trim();
	const passwordValue = password.value.trim();
	const password2Value = password2.value.trim();
	const phone = phone.value.trim();
	const birthday = birthday.value;


	if(usernameValue === '') {
		setErrorFor(username, 'Please insert Username');
	} else {
		setSuccessFor(username);
	}
	
	if(emailValue === '') {
		setErrorFor(email, 'Please insert Email');
	} else if (!isEmail(emailValue)) {
		setErrorFor(email, 'Invalid email');
	} else {
		setSuccessFor(email);
	}
	
	if(passwordValue === '') {
		setErrorFor(password, 'Please insert Password');
	} else {
		setSuccessFor(password);
	}
	
	//if(password2Value === '') {
		//setErrorFor(password2, 'Password2 cannot be blank');
	//} 
	if(password2Value !== passwordValue) {
		setErrorFor(password2, 'Passwords does not match');
	} else{
		setSuccessFor(password2);
	}
	
	if(phoneValue === '') {
		setErrorFor(phone, 'Please insert Phone Number');
	} else if (!isPhone(phoneValue)) {
		setErrorFor(phone, 'Invalid Number');
	} else {
		setSuccessFor(phone);
	}	

	if(birthdayValue === '') {
		setErrorFor(birthday, 'Please insert Birthday');
	} else if (!isBirthday(birthdayValue)) {
		setErrorFor(birthday, 'Invalid email');
	} else {
		setSuccessFor(birthday);
	}
	}
function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
//function to verify email
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}
//function to verify phone number
function isPhone(phone) {
	return /^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/.test(phone);
}
//function to verify birthday
function isEmail(birthday) {
	return insertreqs.test(birthday);
}